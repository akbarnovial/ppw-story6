from .models import Status
from .forms import StatusForm
from django.shortcuts import render,redirect
from . import forms

# Create your views here.

def status(request):
	stats = Status.objects.all()
	if (request.method == 'POST'):
		form = forms.StatusForm(request.POST)
		if (form.is_valid()):
			form.save()
			return redirect('/')
	else:
		form = forms.StatusForm()
	return render(request, 'pages/index.html', {'stats': stats, 'form':form})
