from django.test import TestCase
from .models import Status
from django.test import Client
from django.urls import resolve
from .views import *
# Create your tests here.

class Yogs(TestCase):
    def test_response404(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code,200)

    def test_story6_using_to_do_list_template(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'pages/index.html')

    def test_story6_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, status)

    def test_model_can_create_new_activity(self):
        new_activity = Status.objects.create(status = 'Steak')
        counting_all_available_activity = Status.objects.all().count()
        self.assertEqual(counting_all_available_activity,1)

    def test_can_save_a_POST_request(self):
        response = self.client.post('/', data = {'status': 'Pesan'})

        counting_all_available_activity = Status.objects.all().count()
        self.assertEqual(counting_all_available_activity,1)

        self.assertEqual(response.status_code,302)
        self.assertEqual(response['location'], '/')

        new_response = self.client.get('/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('Pesan', html_response)
